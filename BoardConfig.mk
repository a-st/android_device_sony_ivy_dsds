# Copyright 2014 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include device/sony/kitakami-common/PlatformConfigOmni.mk

TARGET_SPECIFIC_HEADER_PATH += device/sony/ivy_dsds/include

TARGET_BOOTLOADER_BOARD_NAME := E6533

BOARD_KERNEL_CMDLINE += androidboot.hardware=ivy_dsds

WIFI_BUS := PCIE

TARGET_TAP_TO_WAKE_NODE := "/sys/devices/virtual/input/clearpad/wakeup_gesture"

# Compile stock
BUILD_KERNEL := true
TARGET_KERNEL_CONFIG := aosp_kitakami_ivy_defconfig  
TARGET_KERNEL_SOURCE := kernel/sony/kernel

#MultiROM config. MultiROM also uses parts of TWRP config
TARGET_RECOVERY_IS_MULTIROM := true
MR_FSTAB := device/sony/ivy_dsds/rootdir/twrp.fstab
MR_DPI := xhdpi
MR_DPI_FONT := 340
MR_DEV_BLOCK_BOOTDEVICE := true
MR_INPUT_TYPE := type_b
MR_INIT_DEVICES := device/sony/ivy_dsds/multirom/mr_init_devices.c
MR_USE_QCOM_OVERLAY := true
MR_QCOM_OVERLAY_HEADER := device/sony/ivy_dsds/multirom/mr_qcom_overlay.h
MR_FSTAB := device/sony/ivy_dsds/rootdir/twrp.fstab
MR_KEXEC_MEM_MIN := 0x0
MR_DEVICE_HOOKS := device/sony/ivy_dsds/multirom/mr_hooks.c
MR_DEVICE_HOOKS_VER := 5
MR_USE_MROM_FSTAB := true
MR_QCOM_OVERLAY_CUSTOM_PIXEL_FORMAT := MDP_RGBX_8888
MR_ALLOW_NKK71_NOKEXEC_WORKAROUND := true
MR_PIXEL_FORMAT := "RGBX_8888"
